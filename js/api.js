import {listen} from './utils/dispatch';
import {Puzzle} from './game/puzzle';
import Store from '@redom/store';

const DEFAULTS = {
  size: 4
};

export const api = (app) => {
  const store = new Store();
  let updating;
  let puzzle;

  const set = (path, value) => {
    store.set(path, value);
    updating || (updating = window.requestAnimationFrame(() => {
      updating = false;
      app.update(store.get());
    }));
  };

  const reset = (size = DEFAULTS.size) => {
    puzzle = new Puzzle(size);
    set('size', size);
    set('grid', puzzle.getGrid());
    set('win', false);
  };

  listen(app, {
    init(size) {
      reset(size);
    },
    shuffle() {
      set('win', false);
      puzzle.shuffleAsync((data) => {
        set('grid', data);
      });
    },
    randomize() {
      puzzle.randomize();
      set('win', false);
      set('grid', puzzle.getGrid());
    },
    swap(cell) {
      puzzle.swap(cell);
      set('grid', puzzle.getGrid());
      set('win', puzzle.checkWin());
    }
  });

  reset();
};
