'use strict';

import {doAsync, generateGrid, generateRandomGrid} from './helpers';

const ADJACENT = [[0, -1], [0, 1], [-1, 0], [1, 0]];

export class Puzzle {
  constructor(size = 4) {
    this.size = size;
    this.grid = generateGrid(size);
    this.emptyPos = {row: size - 1, col: size - 1};
    this.win = false;
  }

  _getAdjacentTiles(cell) {
    return ADJACENT.map(it => {
      const row = cell.row + it[0];
      const col = cell.col + it[1];
      if (row < this.size && row > -1 && col < this.size && col > -1) {
        return {row, col};
      }
      return null;
    }).filter(it => it !== null);
  }

  _makeSolvable() {
    // check if solvable
    // see http://mathworld.wolfram.com/15Puzzle.html for info
    let n = 0;
    const gridArray = [].concat.apply([], this.grid);
    for (let i = 1; i < gridArray.length - 1; i++) {
      for (let j = i - 1; j >= 0; j--) {
        if (gridArray[j] > gridArray[i]) {
          n++;
        }
      }
    }
    if (n % 2 === 0) {
      // should be solvable
      return;
    }
    // swap two tiles
    const tmp = this.grid[0][0];
    this.grid[0][0] = this.grid[0][1];
    this.grid[0][1] = tmp;
  }

  getGrid() {
    return this.grid;
  }

  checkWin() {
    let prev = 0;
    let i, j;
    for (i = 0; i < this.size; i++) {
      for (j = 0; j < this.size; j++) {
        const curr = this.grid[i][j];
        if (i === this.size - 1 && j === this.size - 1) {
          return curr === '';
        }
        if (curr - prev !== 1) {
          return false;
        }
        prev = curr;
      }
    }
    return true;
  }

  swap(cell, skipWin) {
    const {row, col} = cell;
    if (this.emptyPos.col === col && this.emptyPos.row === row) {
      // can't swap with itself
      return;
    }
    if (Math.abs(row - this.emptyPos.row) + Math.abs(col - this.emptyPos.col) > 1) {
      // disallow to swap
      return;
    }
    this.grid[this.emptyPos.row][this.emptyPos.col] = this.grid[row][col];
    this.grid[row][col] = '';
    this.emptyPos = {
      row, col
    };
    if (!skipWin) {
      this.win = this.checkWin();
    }
  }

  randomize() {
    this.grid = generateRandomGrid(this.size);
    this._makeSolvable();
  }

  shuffleAsync(progress) {
    // obviously we want fancy animations and 100% correct board
    const max = 4 * this.size * Math.pow(2, this.size);
    const step = Math.ceil(max / 256);
    const loop = (skip) => {
      let i;
      for (i = 0; i < step; i++) {
        const adjacent = this._getAdjacentTiles(this.emptyPos);
        const fairDice = Math.floor(Math.random() * adjacent.length);
        this.swap(adjacent[fairDice], true);
      }
      skip += step;
      if (skip < max) {
        doAsync(loop.bind(this, skip))
      }
      progress(this.grid);
    };
    loop(0);
  }
}
