'use strict';

const shuffleArray = (arr) => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
};

export const generateGrid = (size = 4) => {
  const grid = [];
  for (let i = 0; i < size; i++) {
    grid[i] = [];
    for (let j = 0; j < size; j++) {
      grid[i][j] = i * size + j + 1;
    }
  }
  grid[size - 1][size - 1] = '';
  return grid;
};

export const generateRandomGrid = (size = 4) => {
  let numbers = Array.from(Array(size * size).keys());
  numbers.shift();
  numbers.push('');
  numbers = numbers.sort(() => {
    return Math.random() - 0.5;
  });
  numbers = shuffleArray(numbers);
  const grid = [];
  for (let i = 0; i < numbers.length; i = i + size) {
    grid.push(numbers.slice(i, i + size));
  }
  return grid;
};

export const doAsync = typeof window === 'undefined' ? setImmediate : window.requestAnimationFrame;
