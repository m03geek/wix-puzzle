import {el} from 'redom';
import {Grid} from './game-grid';
import {dispatch} from '../utils/dispatch';

export class App {
  constructor() {
    this.el = el('.game',
      this.puzzle = new Grid(),
      el('.game-menu',
        'Size:',
        this.size = el('input', {
          type: 'number',
          max: 10, // cause it looks ugly if more
          min: 3, // cause it looks stupid if less
          oninput: e => dispatch(this, 'init', this.size.value)
        }),
        this.shuffle = el('a.button', {
          onclick: e => dispatch(this, 'randomize')
        }, 'Randomize'),
        this.shuffle = el('a.button', {
          onclick: e => dispatch(this, 'shuffle')
        }, 'Shuffle')
      )
    );
  }

  update(data) {
    const {grid, size, win} = data;
    if (grid) {
      this.puzzle.update(grid);
    }
    if (size) {
      this.size.value = size;
    }
    if (win) {
      window.requestAnimationFrame(() => {
        window.alert('Hurray!');
      });
    }
    this.data = data;
  }
}
