'use strict';

import {list} from 'redom';
import {Row} from './game-row';

export class Grid {
  constructor() {
    this.el = list('table.puzzle', Row);
  }

  update(data) {
    this.el.update(data);
  }
}

