'use strict';

import {el} from 'redom';
import {dispatch} from '../utils/dispatch';

export class Cell {
  constructor() {
    this.el = el('td.puzzle-cell', {
      onclick: e => {
        dispatch(this, 'swap', this.el.data);
      }
    });
  }

  update(data, index, items, context) {
    if (data != this.el.textContent) {
      // avoid redraw when it's not needed
      this.el.textContent = data;
    }
    this.el.data = {
      row: context.row,
      col: index
    };
  }
}
