'use strict';

import {list} from 'redom';
import {Cell} from './game-cell';

export class Row {
  constructor() {
    this.el = list('tr.puzzle-row', Cell);
  }

  update(data, index) {
    this.el.update(data, {row: index});
  }
}

