# Puzzle game

## Requirements

> `>=node-8.0.0`

> `>=npm-5.0.0`

## How-to

**IMPORTANT NOTE**:
To see controls scroll the page down!

* Optionally set game board size
* Remember initial state (that's will be your target)

Easy mode:
* Click on a number adjacent to empty tile.
* Click on a same number again.
* You won! Congratulations!

Hard mode:
* Shuffle/Randomize board
* Use your brains to move all numbers to initial state

### Start
```
cd <GameDir>
npm start
```

### Build
```
npm i
npm run build
npm start
```

### Dev
```
npm i
npm run dev
```

### Test
```
npm i
npm run test
```

## UI Description

* Size - size of game board

* Shuffle - shuffles game tiles (exactly like player do) with fancy animations

* Randomize - generates new random board