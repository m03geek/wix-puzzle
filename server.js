const express = require('express');
const compression = require('compression');
const opn = require('opn');
const app = express();

app.use(compression());
app.use(express.static('public'));

const port = 8765;

app.listen(port, (err) => {
  if (err) {
    console.error('This high-end masterpiece requires port 8765. Please free it and run again.');
    process.exit(1);
  }
  opn(`http://localhost:${port}`);
});
