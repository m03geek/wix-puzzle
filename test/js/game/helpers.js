'use strict';
import {assert, expect} from 'chai';
import {generateGrid, generateRandomGrid} from '../../../js/game/helpers';

describe('helpers', () => {
  const size = 3;
  let numbers = Array.from(Array(size * size).keys());
  numbers.shift();
  numbers.push('');
  const grid = [];
  for (let i = 0; i < numbers.length; i = i + size) {
    grid.push(numbers.slice(i, i + size));
  }

  describe('generateGrid', () => {
    it('should generate initial grid', () => {
      const generated = generateGrid(size);
      assert.deepEqual(grid, generated);
    });
  });

  describe('generateRandomGrid', () => {
    it('should generate random grid', () => {
      const generated = generateRandomGrid(size);
      assert.deepEqual(numbers.sort(), [].concat(...generated).sort());
    });
  });
});
