'use strict';

'use strict';
import {assert} from 'chai';
import {Puzzle} from '../../../js/game/puzzle';

describe('puzzle', () => {
  const size = 3;
  let numbers = Array.from(Array(size * size).keys());
  numbers.shift();
  numbers.push('');
  const grid = [];
  for (let i = 0; i < numbers.length; i = i + size) {
    grid.push(numbers.slice(i, i + size));
  }

  describe('getGrid', () => {
    const puzzle = new Puzzle(3);
    it('should return initial grid', () => {
      assert.deepEqual(grid, puzzle.getGrid());
    });
  });

  describe('checkWin', () => {
    const puzzle = new Puzzle(3);
    it('should win for initial grid', () => {
      assert.equal(true, puzzle.checkWin());
    });
    it('should not win after swap', () => {
      puzzle.swap({row: 1, col: 2});
      assert.equal(false, puzzle.checkWin());
    });
  });

  describe('swap', () => {
    const puzzle = new Puzzle(3);
    const newGrid = JSON.parse(JSON.stringify(grid));
    newGrid[2][2] = 6;
    newGrid[1][2] = '';

    it('should swap tile with empty initial grid', () => {
      puzzle.swap({row: 1, col: 2});
      assert.deepEqual(newGrid, puzzle.getGrid());
    });
    it('shouldn\'t swap with itself', () => {
      puzzle.swap({row: 1, col: 2});
      assert.deepEqual(newGrid, puzzle.getGrid());
    });
  });

  describe('randomize', () => {
    const puzzle = new Puzzle(3);
    puzzle.randomize();
    it('should return random initial grid', () => {
      assert.notDeepEqual(grid, puzzle.getGrid());
    });
    it('should have all numbers', () => {
      assert.deepEqual(numbers.sort(), [].concat(...puzzle.getGrid()).sort());
    });
  });

  describe('shuffleAsync', () => {
    const puzzle = new Puzzle(3);
    it('should shuffle initial grid', (done) => {
      let once = true;
      puzzle.shuffleAsync((shuffled) => {
        assert.notDeepEqual(grid, shuffled);
        if (once) {
          once = false;
          done();
        }
      });
    });
    it('should have all numbers', () => {
      assert.deepEqual(numbers.sort(), [].concat(...puzzle.getGrid()).sort());
    });
  });
});